#include "s21_math.h"

long double s21_atan(double x) {
  long double y;
  if (x == x && x != S21_POS_INF && x != S21_NEG_INF) {
    long double a = x;
    if (s21_fabs(x) > 1) {
      a = 1 / x;
    }  // a =  pi/2 - acrtg 1/x
    if (x < 0.0) a = -a;
    int elem_num = 1;
    int math_error = 1;  //флаг погрешности, если он поднят, то мы прожолжаем
                         //считать следующий элемент
    y = a;  // a - 0 (на самом деле первый элемент) в формуле это просто х
    long double numerator = a;
    while (math_error) {
      numerator =
          (-1.0) * a * a * numerator;  // numerator - числитель, каждая итерация
                                       // - это умножение предыдущего числителя
                                       // на -х2, а первый числитель просто х
      long double current_num =
          numerator /
          (2 * elem_num++ +
           1);  // current_num - текущий член, который потом добавим к y
      // числитель / 2*номер члена +1, т е знаменатель первого после х члена
      // будет 2*1+1 = 3, второго 2*2+1 = 5 и т д
      if (s21_fabs(current_num) < PRESICION)
        math_error = 0;  // проверка на точность до 20 знака после запятой
      // если наш элемент становится меньше, чем погрешность
      y += current_num;
    }
    if (s21_fabs(x) > 1) {
      y = S21_PI / 2 - y;
    }  // pi/2 - 1/x; 1/x мы указывали в 10 строке
    if (x < 0) {
      y *= -1;
    }  // arctg(-x) = -arctg(x)
  }
  if (x != x) {
    y = S21_NAN;
  }
  if (x == S21_POS_INF) {
    y = S21_PI / 2;
  }
  if (x == S21_NEG_INF) {
    y = -S21_PI / 2;
  }
  return y;
}
#include <stdio.h>

#include "s21_math.h"

long double s21_log(double x) {
  long double result = 0, argument = ((x - 1) / (x + 1)), added = argument,
              divider = 1.0;
  int inf_nan_result = inf_nan(x);
  if (inf_nan_result || x <= 0) {
    if (inf_nan_result == 1 || inf_nan_result == 3 || x < 0) {
      result = S21_NAN;
    }
    if (inf_nan_result == 2) {
      result = S21_POS_INF;
    }
    if (x == 0) {
      result = S21_NEG_INF;
    }
  } else {
    while (s21_fabs((double)(added / divider)) > 0.00000000001) {
      result += 2 * (added / divider);
      divider += 2;
      added = added * argument * argument;
    }
  }
  return result;
}
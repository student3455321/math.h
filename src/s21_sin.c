#include "s21_math.h"

long double s21_sin(double x) {
  if (inf_nan(x) != 0) {
    return S21_NAN;
  }
  double t = x;
  double sin_x = t;
  for (int a = 1; a < E; ++a) {
    double mult = -1 * x * x / ((2 * a + 1) * (2 * a));
    t *= mult;
    sin_x += t;
  }

  return sin_x;
}
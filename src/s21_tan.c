#include "s21_math.h"

long double s21_tan(double x) {
  if (inf_nan(x) != 0) {
    return S21_NAN;
  }
  return s21_sin(x) / s21_cos(x);
}
#include "s21_math.h"

long double s21_asin(double x) {
  long double res = 0;
  if (x == x && x != S21_POS_INF && x != S21_NEG_INF) {
    if (x == -1.0) {
      res = -0.5 * S21_PI;
    }
    if (x == 1.0) {
      res = 0.5 * S21_PI;
    }
    if (x < 1 && x > -1) {
      res = s21_atan(x / s21_sqrt(1.0 - (x * x)));
    }  // arcsin = arctg(x/sqrt(1-x*x))
  }
  if (x > 1 || x < -1 || x != x) {
    res = S21_NAN;
  }
  return res;
}
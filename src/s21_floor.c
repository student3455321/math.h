#include "s21_math.h"

long double s21_floor(double x) {
  int inf_nan_result = inf_nan(x);
  long double result = 0;
  if (inf_nan_result == 2) {
    result = S21_POS_INF;
  } else if (inf_nan_result == 3) {
    result = S21_NEG_INF;
  } else {
    long long a = (long long)x;
    if (a != x && x < 0) {
      a--;
    }
    result = a;
  }
  return result;
}
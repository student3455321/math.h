#include "s21_math.h"

long double s21_fmod(double x, double y) {
  long double result = 0;
  if (x == 0 &&
      (y != 0 &&
       inf_nan(y) !=
           1)) {  // fmod(+-0, y) returns +-0 if y is neither 0 nor NaN.
    result = x;
  } else if (inf_nan(x) == 2 || inf_nan(x) == 3 ||
             y == 0) {  // fmod(x, y) returns a NaN and raises the "invalid"
                        // floating-point exception for x infinite or y zero.
    result = S21_NAN;
  } else if ((inf_nan(y) == 2 || inf_nan(y) == 3) &&
             (inf_nan(x) != 2 &&
              inf_nan(x) !=
                  3)) {  // fmod(x, +-infinity) returns x for x not infinite.
    result = x;
  } else {
    double absolut_x = s21_fabs(x);
    result = (absolut_x - ((int)(absolut_x / y)) * y) * (absolut_x / x);
  }
  return result;
}
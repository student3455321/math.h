#include "s21_math.h"

long double s21_acos(double x) {
  long double res;
  if (x < -1 || x > 1) {
    res = S21_NAN;
  } else {
    res = S21_PI / 2 - s21_asin(x);
  }  // arcsin x + arccos x = π/2
  return res;
}
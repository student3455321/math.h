#include "s21_math.h"

long double s21_sqrt(double x) {
  double guess = x;
  int inf_nan_result = inf_nan(x);
  if (inf_nan_result || x < 0) {
    if (inf_nan_result == 2) {
      guess = S21_POS_INF;
    } else {
      guess = S21_NAN;
    }
  } else {
    while (s21_fabs(guess - x / guess) > EPS20) {
      guess = s21_fabs(guess + x / guess) / 2;
    }
  }
  return guess;
}
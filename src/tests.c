#include <check.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "s21_math.h"

START_TEST(s21_fabs_test) {  // done
  for (double x = -100; x <= 100; x += 0.001) {
    ck_assert_ldouble_eq_tol(s21_fabs(x), fabs(x), PRESICION);
  }
  for (double x = 1; x > -1; x -= 0.001) {
    ck_assert_ldouble_eq_tol(s21_fabs(x), fabs(x), PRESICION);
  }
  ck_assert_ldouble_infinite(s21_fabs(INFINITY));
  ck_assert_ldouble_infinite(s21_fabs(-INFINITY));
}
END_TEST

START_TEST(s21_abs_test) {  // done
  for (int x = -100; x <= 100; x++) {
    ck_assert_int_eq(s21_abs(x), abs(x));
  }
}
END_TEST

START_TEST(s21_floor_test) {  // done
  for (double x = -100; x <= 100; x += 0.001) {
    ck_assert_ldouble_eq_tol(s21_floor(x), floor(x), PRESICION);
  }
  for (double x = 1; x > -1; x -= 0.001) {
    ck_assert_ldouble_eq_tol(s21_floor(x), floor(x), PRESICION);
  }
  ck_assert_ldouble_infinite(s21_floor(INFINITY));
  ck_assert_ldouble_infinite(s21_floor(-INFINITY));
}
END_TEST

START_TEST(s21_ceil_test) {  // done
  for (double x = -100; x <= 100; x += 0.1) {
    ck_assert_ldouble_eq_tol(s21_ceil(x), ceil(x), PRESICION);
  }
  for (double x = 1; x >= -1; x -= 0.01) {
    ck_assert_ldouble_eq_tol(s21_ceil(x), ceil(x), PRESICION);
  }
  ck_assert_ldouble_infinite(s21_ceil(INFINITY));
  ck_assert_ldouble_infinite(s21_ceil(-INFINITY));
}
END_TEST

START_TEST(s21_asin_test) {
  for (double x = -1; x <= 1; x += 0.001) {
    ck_assert_ldouble_eq_tol(s21_asin(x), asin(x), PRESICION);
  }
  long double asin_from_1 = s21_asin(1);
  long double asin_from_1_orig = asin(1);
  ck_assert_ldouble_eq_tol(asin_from_1, asin_from_1_orig, PRESICION);

  long double asin_from_posbig = s21_asin(10);
  long double asin_from_negbig = s21_asin(-10);
  long double asin_from_infinity = s21_asin(INFINITY);
  ck_assert_ldouble_nan(asin_from_posbig);  // add -infinity
  ck_assert_ldouble_nan(asin_from_negbig);
  ck_assert_ldouble_nan(asin_from_infinity);
}
END_TEST

START_TEST(s21_acos_test) {
  for (double x = -1; x < 1; x += 0.001) {
    ck_assert_ldouble_eq_tol(s21_acos(x), acos(x), PRESICION);
  }
  long double acos_from_1 = s21_acos(1);
  long double acos_from_1_orig = acos(1);
  ck_assert_ldouble_eq_tol(acos_from_1, acos_from_1_orig, PRESICION);

  long double acos_from_posbig = s21_acos(10);
  long double acos_from_negbig = s21_acos(-10);
  long double acos_from_infinity = s21_acos(INFINITY);
  ck_assert_ldouble_nan(acos_from_posbig);
  ck_assert_ldouble_nan(acos_from_negbig);
  ck_assert_ldouble_nan(acos_from_infinity);
}
END_TEST

START_TEST(s21_atan_test) {
  for (double x = -10; x <= 10; x += 0.001) {
    // if (s21_fabs(x + 1) >= PRESICION && s21_fabs(x - 1) >= PRESICION) {
    ck_assert_ldouble_eq_tol(s21_atan(x), atan(x), PRESICION);
    // }
  }
  long double atan_from_infinity = s21_atan(INFINITY);
  long double atan_from_infinity_orig = atan(INFINITY);
  ck_assert_ldouble_eq_tol(atan_from_infinity, atan_from_infinity_orig,
                           PRESICION);
}
END_TEST

START_TEST(s21_cos_test) {
  for (double x = -2 * M_PI; x <= 2 * M_PI; x += 0.01) {
    ck_assert_ldouble_eq_tol(s21_cos(x), cos(x), PRESICION);
  }
  long double cos_from_infinity = s21_cos(INFINITY);
  ck_assert_ldouble_nan(cos_from_infinity);
}
END_TEST

START_TEST(s21_sin_test) {
  for (double x = -2 * M_PI; x <= 2 * M_PI; x += 0.01) {
    ck_assert_ldouble_eq_tol(s21_sin(x), s21_sin(x), PRESICION);
  }
  long double sin_from_infinity = s21_sin(INFINITY);
  ck_assert_ldouble_nan(sin_from_infinity);
}
END_TEST

START_TEST(s21_exp_test) {  // done
  for (double x = -10; x <= 10; x += 0.001) {
    ck_assert_ldouble_eq_tol(s21_exp(x), exp(x), PRESICION);
  }
  long double exp_to_nan = s21_exp(NAN);
  ck_assert_ldouble_nan(exp_to_nan);

  long double exp_to_0 = s21_exp(0);
  long double exp_to_0_orig = exp(0);
  ck_assert_ldouble_eq_tol(exp_to_0, exp_to_0_orig, PRESICION);

  long double exp_to_infinity = s21_exp(INFINITY);
  ck_assert_ldouble_infinite(exp_to_infinity);
}
END_TEST

START_TEST(s21_fmod_test) {  // done
  for (double x = -10; x <= 10; x += 0.1) {
    for (double y = -15; y <= 15; y += 0.1) {
      if (s21_fabs(y) > PRESICION) {
        // printf("%f, %.15f\n", x, y);
        ck_assert_ldouble_eq_tol(s21_fmod(x, y), fmod(x, y), PRESICION);
      }
    }
  }
  long double fmod_0 =
      s21_fmod(0, 120);  // fmod(+-0, y) returns +-0 if y is neither 0 nor NaN.
  long double fmod_0_orig = fmod(0, 120);
  ck_assert_ldouble_eq_tol(fmod_0, fmod_0_orig, PRESICION);

  long double fmod_infinity_tosmthg = s21_fmod(INFINITY, 100);
  long double fmod_neginfinity_tosmthg = s21_fmod(
      -INFINITY, 100);  // fmod(x, y) returns a NaN and raises the "invalid"
                        // floating-point exception for x infinite or y zero.
  long double fmod_smthg_to_0 = s21_fmod(10, 0);
  ck_assert_ldouble_nan(fmod_smthg_to_0);
  ck_assert_ldouble_nan(fmod_infinity_tosmthg);
  ck_assert_ldouble_nan(fmod_neginfinity_tosmthg);

  long double fmod_smthg_to_infinity = s21_fmod(
      100, INFINITY);  // fmod(x, +-infinity) returns x for x not infinite.
  long double fmod_smthg_to_infinity_orig = fmod(100, INFINITY);
  ck_assert_ldouble_eq_tol(fmod_smthg_to_infinity, fmod_smthg_to_infinity_orig,
                           PRESICION);
}
END_TEST

START_TEST(s21_log_test) {  // done
  for (double x = 100; x >= 0; x -= 0.01) {
    ck_assert_ldouble_eq_tol(s21_log(x), log(x), PRESICION);
  }
  ck_assert_ldouble_eq_tol(s21_log(1), log(1), PRESICION);
  ck_assert_ldouble_nan(s21_log(NAN));
  ck_assert_ldouble_infinite(s21_log(INFINITY));
  ck_assert_ldouble_infinite(s21_log(0));
}
END_TEST

START_TEST(s21_sqrt_test) {  // done
  for (double x = -0; x <= 100; x += 0.01) {
    ck_assert_ldouble_eq_tol(s21_sqrt(x), sqrt(x), PRESICION);
  }
  ck_assert_ldouble_nan(s21_sqrt(-1));
  ck_assert_ldouble_nan(s21_sqrt(-INFINITY));
  ck_assert_ldouble_infinite(s21_sqrt(INFINITY));
}
END_TEST

START_TEST(s21_tan_test) {
  for (double x = -2 * M_PI; x <= 2 * M_PI; x += 0.01) {
    ck_assert_ldouble_eq_tol(s21_tan(x), tan(x), PRESICION);
  }
  ck_assert_ldouble_eq_tol(s21_tan(-0), tan(-0), PRESICION);
  ck_assert_ldouble_eq_tol(s21_tan(+0), tan(+0), PRESICION);
  ck_assert_ldouble_nan(s21_tan(INFINITY));
  ck_assert_ldouble_nan(s21_tan(-INFINITY));
}
END_TEST

START_TEST(s21_pow_test) {  // done
  for (long double base = 0.1; base < 5; base += 0.1) {
    for (long double exp = 0.1; exp < 5; exp += 0.1) {
      long double pow_result = s21_pow(base, exp);
      long double pow_result_orig = pow(base, exp);
      ck_assert_ldouble_eq_tol(pow_result, pow_result_orig, PRESICION);
    }
  }
  long double pow_0_to_neg_odd = s21_pow(
      0, -3);  // pow(+-0, y) returns +-infinity and raises the "divide-by-zero"
               // floating-point exception for y an odd integer < 0.
  ck_assert_ldouble_infinite(pow_0_to_neg_odd);

  long double pow_0_to_neg_even = s21_pow(
      0, -4);  // pow(+-0, y) returns +infinity and raises the "divide-by-zero"
               // floating-point exception for y < 0 and not an odd integer.
  ck_assert_ldouble_infinite(pow_0_to_neg_even);
  long double pow_0_to_pos_odd =
      s21_pow(0, 3);  //  pow(+-0, y) returns +-0 for y an odd integer > 0.
  ck_assert_ldouble_eq_tol(pow_0_to_pos_odd, 0, PRESICION);

  long double pow_0_to_pos_even = s21_pow(0, 4);
  ck_assert_ldouble_eq_tol(
      pow_0_to_pos_even, 0,
      PRESICION);  //  pow(+-0, y) returns +0 for y > 0 and not an odd integer.

  long double pow_neg1_to_inf = s21_pow(-1, INFINITY);
  long double pow_neg1_to_inf_orig = pow(-1, INFINITY);
  long double pow_neg1_to_neginf = s21_pow(-1, -INFINITY);
  long double pow_neg1_to_neginf_orig = pow(-1, -INFINITY);
  ck_assert_ldouble_eq_tol(pow_neg1_to_inf, pow_neg1_to_inf_orig,
                           PRESICION);  //  pow(-1, +-infinity) returns 1.
  ck_assert_ldouble_eq_tol(pow_neg1_to_neginf, pow_neg1_to_neginf_orig,
                           PRESICION);

  long double pow_1_to_anypos = s21_pow(1, 0.395);
  long double pow_1_to_anyneg = s21_pow(1, -0.395);
  long double pow_1_to_nan = s21_pow(1, NAN);
  ck_assert_ldouble_eq_tol(pow_1_to_anypos, 1, PRESICION);
  ck_assert_ldouble_eq_tol(pow_1_to_anyneg, 1, PRESICION);
  ck_assert_ldouble_eq_tol(
      pow_1_to_nan, 1,
      PRESICION);  //  pow(1, y) returns 1 for any y, even a NaN.

  long double pow_anypos_to_0 = s21_pow(2323, 0);
  long double pow_anyneg_to_0 = s21_pow(-32.211, 0);
  long double pow_nan_to_0 = s21_pow(NAN, 0);
  ck_assert_ldouble_eq_tol(pow_anypos_to_0, 1, PRESICION);
  ck_assert_ldouble_eq_tol(pow_anyneg_to_0, 1, PRESICION);
  ck_assert_ldouble_eq_tol(
      pow_nan_to_0, 1,
      PRESICION);  //  pow(x, +-0) returns 1 for any x, even a NaN.

  long double pow_anyneg_to_any_negnonint = s21_pow(-12, 0.5);
  long double pow_anyneg_to_any_posnonint = s21_pow(-32.211, -0.03);
  ck_assert_ldouble_nan(pow_anyneg_to_any_negnonint);
  ck_assert_ldouble_nan(
      pow_anyneg_to_any_posnonint);  //  pow(x, y) returns a NaN and raises the
                                     //  "invalid" floating-point exception for
                                     //  finite x < 0 and finite non-integer y.

  long double pow_smallnumber_to_neginfinity = s21_pow(0.5, -INFINITY);
  long double pow_bignumber_to_neginfinity = s21_pow(-32.211, -INFINITY);
  ck_assert_ldouble_infinite(
      pow_smallnumber_to_neginfinity);  //  pow(x, -infinity) returns +infinity
                                        //  for |x| < 1.
  ck_assert_ldouble_eq_tol(
      pow_bignumber_to_neginfinity, 0,
      PRESICION);  //  pow(x, -infinity) returns +0 for |x| > 1.

  long double pow_smallnumber_to_posinfinity = s21_pow(0.5, INFINITY);
  long double pow_bignumber_to_posinfinity = s21_pow(-32.211, INFINITY);
  ck_assert_ldouble_infinite(
      pow_bignumber_to_posinfinity);  //  pow(x, +infinity) returns +infinity
                                      //  for |x| > 1.
  ck_assert_ldouble_eq_tol(
      pow_smallnumber_to_posinfinity, 0,
      PRESICION);  //  pow(x, +infinity) returns +0 for |x| < 1.

  long double pow_neginfinity_to_negodd = s21_pow(-INFINITY, -3);
  long double pow_neginfinity_to_negodd_orig = pow(
      -INFINITY, -3);  // pow(-infinity, y) returns -0 for y an odd integer < 0.
  ck_assert_ldouble_eq_tol(pow_neginfinity_to_negodd,
                           pow_neginfinity_to_negodd_orig, PRESICION);

  long double pow_neginfinity_to_negeven = s21_pow(-INFINITY, -4);
  long double pow_neginfinity_to_negeven_orig = pow(-INFINITY, -4);
  ck_assert_ldouble_eq_tol(pow_neginfinity_to_negeven,
                           pow_neginfinity_to_negeven_orig,
                           PRESICION);  // pow(-infinity, y) returns +0 for y <
                                        // 0 and not an odd integer.

  long double pow_neginfinity_to_posodd = s21_pow(
      -INFINITY,
      3);  // pow(-infinity, y) returns -infinity for y an odd integer > 0.
  ck_assert_ldouble_infinite(pow_neginfinity_to_posodd);

  long double pow_neginfinity_to_poseven = s21_pow(-INFINITY, 4);
  ck_assert_ldouble_infinite(
      pow_neginfinity_to_poseven);  // pow(-infinity, y) returns +infinity for y
                                    // > 0 and not an odd integer.

  long double pow_posinfinity_to_neg = s21_pow(INFINITY, -4);
  long double pow_posinfinity_to_neg_orig = pow(INFINITY, -4);
  ck_assert_ldouble_eq_tol(
      pow_posinfinity_to_neg, pow_posinfinity_to_neg_orig,
      PRESICION);  // pow(+infinity, y) returns +0 for y < 0.

  long double pow_posinfinity_to_pos =
      s21_pow(INFINITY, 4);  // pow(+infinity, y) returns +infinity for y > 0.
  ck_assert_ldouble_infinite(pow_posinfinity_to_pos);
}
END_TEST

Suite *math_test(void) {
  Suite *s;
  TCase *tc_core;
  s = suite_create("s21_math");
  tc_core = tcase_create("Core");
  tcase_add_test(tc_core, s21_fabs_test);
  tcase_add_test(tc_core, s21_abs_test);
  tcase_add_test(tc_core, s21_floor_test);
  tcase_add_test(tc_core, s21_ceil_test);
  tcase_add_test(tc_core, s21_asin_test);
  tcase_add_test(tc_core, s21_acos_test);
  tcase_add_test(tc_core, s21_atan_test);
  tcase_add_test(tc_core, s21_cos_test);
  tcase_add_test(tc_core, s21_sin_test);
  tcase_add_test(tc_core, s21_exp_test);
  tcase_add_test(tc_core, s21_fmod_test);
  tcase_add_test(tc_core, s21_log_test);
  tcase_add_test(tc_core, s21_sqrt_test);
  tcase_add_test(tc_core, s21_tan_test);
  tcase_add_test(tc_core, s21_pow_test);
  suite_add_tcase(s, tc_core);
  return s;
}

int main() {
  int no_failed = 0;
  Suite *s;
  SRunner *runner;
  s = math_test();
  runner = srunner_create(s);
  srunner_run_all(runner, CK_VERBOSE);
  no_failed = srunner_ntests_failed(runner);
  srunner_free(runner);
  no_failed == 0 ? printf("Success\n") : printf("fail\n");
  return 0;
}

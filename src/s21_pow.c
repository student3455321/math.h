#include <stdio.h>

#include "s21_math.h"

long double s21_pow(double base, double exp) {
  long double result = 0.0;
  // printf("%f - base, %f - exp, %d = inf_nan_base, %d = inf_nan_exp, %d == exp
  // < 0, %d == exp %% 2\n", base, exp, inf_nan(base), inf_nan(exp), exp < 0,
  // (int)exp % 2);
  if (base == 0 && is_int(exp)) {
    if (exp > 0.0 &&
        ((int)exp % 2) ==
            0) {  //  pow(+-0, y) returns +0 for y > 0 and not an odd integer.
      result = +0.0;
    }
    if (exp > 0.0 &&
        (int)exp % 2) {  //  pow(+-0, y) returns +-0 for y an odd integer > 0.
      result = base;
    }
    if (exp < 0.0 &&
        ((int)exp % 2) == 0) {  // pow(+-0, y) returns +-infinity and raises the
                                // "divide-by-zero" floating-point exception for
                                // y an odd integer < 0.
      result = S21_POS_INF;
    }
    if (exp < 0.0 &&
        (int)exp % 2) {  // pow(+-0, y) returns +infinity and raises the
                         // "divide-by-zero" floating-point exception for y < 0
                         // and not an odd integer.
      result = S21_POS_INF;  // надо отличить +0 от -0
    }
  } else if (base == -1 &&
             (inf_nan(exp) == 2 ||
              inf_nan(exp) == 3)) {  // pow(-1, +-infinity) returns 1.
    result = 1;
  } else if (base == 1) {  // pow(1, y) returns 1 for any y, even a NaN.
    result = 1;
  } else if (exp == 0) {  // pow(x, +-0) returns 1 for any x, even a NaN.
    result = 1;
  } else if (inf_nan(base) == 0 && base < 0 && is_int(exp) == 0 &&
             !inf_nan(exp)) {  // pow(x, y) returns a NaN and raises the
                               // "invalid" floating-point exception for finite
                               // x < 0 and finite non-integer y.
    result = S21_NAN;
  } else if (s21_fabs(base) < 1 &&
             inf_nan(exp) ==
                 3) {  // pow(x, -infinity) returns +infinity for |x| < 1.
    result = S21_POS_INF;
  } else if (s21_fabs(base) > 1 &&
             inf_nan(exp) == 3) {  // pow(x, -infinity) returns +0 for |x| > 1.
    result = +0.0;
  } else if (s21_fabs(base) < 1 &&
             inf_nan(exp) == 2) {  // pow(x, +infinity) returns +0 for |x| < 1.
    result = +0.0;
  } else if (s21_fabs(base) > 1 &&
             inf_nan(exp) ==
                 2) {  // pow(x, +infinity) returns +infinity for |x| > 1.
    result = S21_POS_INF;
  } else if (inf_nan(base) == 3 && inf_nan(exp) == 0 && exp < 0 &&
             (int)exp % 2 !=
                 0) {  // pow(-infinity, y) returns -0 for y an odd integer < 0.
    result = -0.0;
  } else if (inf_nan(base) == 3 && inf_nan(exp) == 0 && exp < 0 &&
             (int)exp % 2 == 0) {  // pow(-infinity, y) returns +0 for y < 0 and
                                   // not an odd integer.
    result = +0.0;
  } else if (inf_nan(base) == 3 && inf_nan(exp) == 0 && exp > 0 &&
             (int)exp % 2 != 0) {  // pow(-infinity, y) returns -infinity for y
                                   // an odd integer > 0.
    result = S21_NEG_INF;
  } else if (inf_nan(base) == 3 && inf_nan(exp) == 0 && exp > 0 &&
             (int)exp % 2 == 0) {  // pow(-infinity, y) returns +infinity for y
                                   // > 0 and not an odd integer.
    result = S21_POS_INF;
  } else if (inf_nan(base) == 2 && inf_nan(exp) == 0 &&
             exp < 0) {  // pow(+infinity, y) returns +0 for y < 0.
    result = +0.0;
  } else if (inf_nan(base) == 2 && inf_nan(exp) == 0 &&
             exp > 0) {  // pow(+infinity, y) returns +infinity for y > 0.
    result = S21_POS_INF;
  } else {
    result = s21_exp(exp * s21_log(base));
  }
  return result;
}
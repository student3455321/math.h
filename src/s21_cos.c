#include "s21_math.h"

long double s21_cos(double x) {
  if (inf_nan(x) != 0) {
    return S21_NAN;
  }
  long double sin_x = s21_sin(S21_PI / 2 - x);
  return sin_x;
}

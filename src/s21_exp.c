#include "s21_math.h"

long double s21_exp(double x) {
  long double result = 1, added = x;
  int divider = 2;
  int inf_nan_result = inf_nan(x);
  if (inf_nan_result) {
    if (inf_nan_result == 1) {
      result = S21_NAN;
    }
    if (inf_nan_result == 2) {
      result = S21_POS_INF;
    }
  } else {
    while (s21_fabs(added) > PRESICION / 10000) {
      result += added;
      added = added * x / divider;
      divider++;
    }
  }
  return result;
}
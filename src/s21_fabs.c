#include "s21_math.h"

long double s21_fabs(double x) {
  long double ret = (long double)x;
  if (x != x) {
    ret = S21_NAN;
  } else if (x == S21_POS_INF || x == S21_NEG_INF) {
    ret = S21_POS_INF;
  } else if (x <= 0) {
    ret *= -1;
  }
  return ret;
}
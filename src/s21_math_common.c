#include "s21_math.h"

/* Функция возвращает:
    1 (значение nan),
    2 (значение +inf),
    3 (значение -inf) или 0
 */
int inf_nan(double f) {
  float zero = 0.0;
  int res = 0;
  if (f != f)
    res = 1;
  else if (f >= 1.0 / zero)
    res = 2;
  else if (f <= -1.0 / zero)
    res = 3;
  return res;
}

/* Функция ожидает значение n, -n или 0 и соответственно выводит:
    значение +inf, -inf или nan
 */
double out_inf_nan(double x) {
  double zero = 0;
  return (x / zero);
}
/* функция возвращает 1 если инт и 0 если не инт*/
bool is_int(double x) { return x == (int)x; }
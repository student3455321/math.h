#include "s21_math.h"

long double s21_ceil(double x) {
  long double result = 0;
  int inf_nan_result = inf_nan(x);
  if (inf_nan_result == 2) {
    result = S21_POS_INF;
  } else if (inf_nan_result == 3) {
    result = S21_NEG_INF;
  } else {
    long long a = (long long)x;
    if (x > 0 && is_int(x) == 0) {
      a++;
    }
    result = (long double)a;
  }
  return result;
}